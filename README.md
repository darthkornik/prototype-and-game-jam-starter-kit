# Prototype and Game Jam Starter Kit

**Prototype and Game Jam Starter Kit** is an open-source library for helping with quickly prototyping games, creating game jam projects or similar things in Unity.

The primary focus is on ease of use (a.k.a 'it should just work'), **not** performance. Please be conscious of that then when using it in any production code.


## How to best make use of it?

Most scripts and other assets are either drag & drop (like MonoBehaviours that add functionality to a GameObjects) or in the form of a singleton.

And mostly either 'just work' (with optional parameters in the inspector) or require a single line of code to invoke.

**It is intended for the scripts (and other assets) to be modified, extended or copy/past to add desired extra functionality.** For example: `HP` script can be copied and changed to `Stamina` or `Mana` to add a simple stamina or mana functionality to your project.

If you only want the library (and not the example scenes and other stuff) just copy `/Assets/Plugins/Prototype & Game Jam Starter Kit/` to your project.


## What's included?


### CameraFollow2D.cs

Makes camera follow a set target (version intended for 2D games).

`ChangeTarget` method can be used to change the target. Follow speed and camera position offset can be adjusted in the inspector.

Compatible with `ScreenShake.cs`.


### CameraFollow3D.cs

Makes camera follow a set target (version intended for 3D games).

`ChangeTarget` method can be used to change the target. Follow speed and camera position offset can be adjusted in the inspector.

Compatible with `ScreenShake.cs`.


### CustomCursor.cs

Used to create a custom cursor.

**Has to be added to a GameObject on a Screen Space canvas. Needs an image for the cursor itself and optional image for the inside. Both need to be a children of the GameObject the script is on.**

The inside image can change color in respons to a mouse click.

The class is a singelton and has a method `OnInteraction( )` that can be called to make the cursor scale on interactions.

It also has many optional parameters accessible via the Inspector window.


### DestroyOnTimer.cs

Destroys GameObject after a set number of seconds. Optionally children can be detached before the destruction.


### DetectObjectInRange.cs

Detects object using 2 types of detection modes:

* Distance - detects object in specified distance (`RangeOfDetection` field in the inspector)
* Trigger - detects object in trigger (needs component of type `Collider2D`, for example: `CircleCollider2D`, `BoxCollider2D` etc.)

There are also 2 types of event 'invoke delay' modes:

* Every frame - invokes event every frame 
* Time - invokes event every couple of seconds (`InvokeDelayTime` field in the inspector)

You can set tags that will be detected by script in Detection Tags list.


### FloatingText.cs

Allows to easely create floating text know from action or RPG games (used to show damage or healing).

Add to a world space canvas with TextMeshPro text attached to it to make it work.

Can be spawn and set up using code like this:

```c#
GameObject go = Instantiate( floatingText, transform.position, Quaternion.identity );
FloatingText ft = go.GetComponent<FloatingText>( );
ft.SetPrameters( textToShow, 1.0f, 1.0f, Color.white );
```

Has a rich set of parameters that can be set via the inspector.


### GameControl.cs

Add to any GameObject. Allows to exit the game (`Esc` key) and/or reset the current scene  (`R` key).

Use of the `Esc` key in the Editor stops playback.


### HP.cs

Adds health functionality to a GameObject (or after renaming, a similar functionality like mana or stamina).

Add to a GameObject that should have HP. Use UnityEvents to get notifications on events: `onHealthChange` and `onDeath`. Start HP can be adjusted in the inspector.

Please use public methods:

* `TakeDamage` to remove health
* and `Heal` to add health
 
Public property `CurrentHP` returns the amount of current health, and `MaxHP` returns the maximum/starting health.

Attach a standard Unity UI Slider in the inspector to add a simple HP Bar functionality. Another one adds an 'under bar' effect. Attaching a TextMeshPro text will add a health amount label.


### Interaction.cs

Adds an ability to perform interactions after pressing a key. Attach to `onInteraction` UnityEvent to act on the interaction.

You can set a key that invokes the interaction.


### LogEvents.cs

Has few different overloaded methods that provide easy way for more distinct and readable console logs. Useful for more important messages.

Example usages:
- `this.LogEvent( "Bob", "got damaged", 10, false );`
- `this.LogEvent( "Bob", "got healed", 5 );`
- `this.LogEvent( "Bob", "got killed" );`
- `this.LogEvent( "Button pressed", false, true );`
- `this.LogEvent( "Button pressed" );`


### Movement2D.cs

Simple movement script. Can be set to Top Down Mode or Platformer Mode, for use in respective game types.


### PlayerMovement2D.cs

Moves player type entity using physics forces. Max speed, force applied and other parameters can be adjusted via the inspector.

Compared to `Movement2D.cs` works better with physics interactions. But more suited to top-down games out of the box.

**For best movement experience please set Axes parameters (in Input Manager)**
* Gravity = 3000
* Sensitivity = 300


### PlaySound.cs

Plays a sound:

* on instantiation of a GameObject
* on scene load
* on `Play()` method call
* on `Play(float pitch)` method call
 
Useful as a component of a GameObject instantiated on some event (like on shot, on explosion or on enemy death).

Requires an `AudioSource` attached to the same GameObject (created automatically) and at least one `Sound Clip` added to `Sounds` field.

Has the ability to randomize Pitch and Volume. As well as limit min. time between consecutive plays.

If many `Sound Clips` are added it will randomly play one of them.


### PostProcessingEffects.cs

Quick post-processing effects using code. **Requires post-processing stack to be added and configured on the scene.**

This class is singleton and currently has effects listed below (more to come):
- `Explosion(float strength)` - adds more power to explosion effects by temporally increasing Bloom and Chromatic Aberration 

Has a rich set of parameters that can be set via the inspector.

Uses `Mathf.Lerp` for changes over time but has commented code that allows it to use `LeanTween` (for example to use different tweening functions).


### RandomPositionOnCircleOrSphere.cs

When added to the project, gives access to methods which allow you to generate random position both on a circle and a sphere.


### RotateTowards2D.cs

Rotates GameObject either towards a `target` (if provided) or towards mouse position (if `target` is null). Speed of rotation can be adjusted.


### ScaleChanger.cs

Changes the scale of the object.

Can be run on start (level load or object instantiation) or on demand using `StartScale()` method.

Has a rich set of parameters that can be set via the inspector.

Uses `Vector3.Lerp` but has commented code that allows it to use `LeanTween` for scaling (for example to use different tweening functions).


### ScreenShake.cs

Shakes the camera it is attached to, simulating effects seen in movies.

To quickly invoke the screen shake use the following "shortcut" methods:

* `ScreenShake.Instance.StartScreenShake2D(float duration, float moveStrength, float rootZStrength);`
* `ScreenShake.Instance.StartScreenShake3D(float duration, float moveStrength, float rootZStrength);`
* `ScreenShake.Instance.StartScreenShakeBasic(float duration, float moveStrength, float rootStrength);`

Or use `StartScreenShake` to adjust each and every parameter available individually.

`CallScreenShake.cs` is provided for an easier use via the inspector.

Screen shake is compatible with CameraFollow2D and CameraFollow3D.


### SetEnabled.cs

Allows to have something disabled (like the pesky UI) while editing a scene but enabled when you press Play.

It can also work in reverse: disables something enabled on game start.


### ShowHide.cs

Shows and/or hides GameObjects. Does it on OnEnable and/or OnDisable.

Useful for example when you want something disabled while working on the game but at the same to have it enabled while the game runs.

Can also be used to disable or enable other GameObjects in respons to disabling/enabling GameObject with this script on.


### SpawnSomething.cs

Spawn GameObject assigned to `Thing To Spawn` in the inspector in respons to one of the method calls:

* `Spawn()` - spawn at the script position
* `Spawn(Vector2 position)` - spawns at desired position
* `Spawn(Vector2 position, Quaternion rotation)` - spawns at desired position with desired rotation

Mostly useful for calling from `UnitEvents`.


### Sprites

`Sprites' folder contains a set of useful placeholder sprites:

* Circles
* Item placeholders (few versions)
* Squares
* Stairs
* Triangles

Each sprite is available in 3 sizes.


### SpritesBlink.cs

Does a 'blink': an effect (from the 'good old days') where a sprite flashes, for example, white in response to external event, usually damage.

The `Blink Material`, `Normal Material`, `Duration`,  `Blink Amount`,  and affected `Sprites` can be assigned and adjusted in the inspector. Optionally sprites can change color when blinking (please use `Blink Color` and `Change Color`).

There are two modes:

* Change - the material is changed
* Hide - the sprites are hidden for the duration of the blink

By default all sprites on a GameObject are affected, if none are assigned explicitly.

**Before use please assign the materials if using Change Mode.** An example blink material has been provided in the `Materials` folder. As `Normal Material` the build-in default sprite material should be a good start.


### VolumeControl.cs

After attaching a Slider and an AudioMixer allows to control volume.

**``Exposed Property`` in inspector must match the one in Audio Mixer.**

Has optional (commented) code to that allows to control the volume of FMOD bus.


### AssertTool.cs

Contains method which can be used for Assert messages