using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFieldChecker : MonoBehaviour
{
	private GameObject _notNullObject = null;
	private GameObject _testNullObj = null;

	private List<GameObject> _nullListTest = null;
	private GameObject[] _nullArrayTest = null;

	private List<GameObject> _emptyListTest = new List<GameObject>();
	private GameObject[] _emptyArrayTest = new GameObject[0];

	private List<int> _notEmptyList = new List<int>() { 1 };
	private int[] _notEmptyArray = new int[] { 1 };

	void Start()
	{
		_notNullObject = gameObject;
		Debug.LogWarning("Testing property field checker");

		FieldChecker.IsNotNull(this,
			nameof(_notNullObject),
			nameof(_testNullObj)
			);

		FieldChecker.ArrayIsNotEmpty(this,
			nameof(_nullListTest),
			nameof(_nullArrayTest),
			nameof(_emptyListTest),
			nameof(_emptyArrayTest),
			nameof(_notEmptyList),
			nameof(_notEmptyArray)
			);
		Debug.LogWarning("Finished testing property field checker");
	}
}
