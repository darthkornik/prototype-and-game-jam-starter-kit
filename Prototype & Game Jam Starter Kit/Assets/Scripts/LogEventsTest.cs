﻿/**
 * Description: Tests LogEvents.
 * Authors: Kornel
 * Copyright: © 2019 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using UnityEngine;

public class LogEventsTest : MonoBehaviour
{
	public void DoDamage ()
	{
		this.LogEvent( "Bob", "got damaged", 10, false );
	}

	public void Heal ()
	{
		this.LogEvent( "Bob", "got healed", 5 );
	}

	public void Inform ()
	{
		this.LogEvent( "Bob", "got killed" );
	}

	public void SimpleInform ()
	{
		this.LogEvent( "Button pressed", false, true );
	}
}
