﻿/**
 * Description: Tests screen shake - press "K" key to start it.
 * Authors: Michał Wildanger, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio.All rights reserved.For license see: 'LICENSE' file.
**/

using UnityEngine;

public class InvokeScreenShakeByButton : MonoBehaviour
{
    [SerializeField] private ScreenShake.ShakeType screenShakeType = ScreenShake.ShakeType.Common;

	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            ScreenShake.Instance.StartScreenShake(screenShakeType, 0.5f, new Vector3(1, 1, 1), new Vector3(1, 1, 1));
        }
	}
}
