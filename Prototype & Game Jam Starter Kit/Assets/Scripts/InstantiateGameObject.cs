﻿/**
 * Description: Instantiates a GameObject.
 * Authors: Kornel, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
 **/

using UnityEngine;
using UnityEngine.Assertions;

public class InstantiateGameObject : MonoBehaviour
{
	[SerializeField] private GameObject objectToInstantiate = null;

	void Start ()
	{
		Assert.IsNotNull( objectToInstantiate );
	}

	/// <summary>
	/// Instantiates the assigned GameObject.
	/// </summary>
	public void InstantiateObject( )
	{
		Instantiate( objectToInstantiate, transform.position, Quaternion.identity );
	}
}
