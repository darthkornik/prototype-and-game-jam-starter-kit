﻿/**
 * Description: Hides/shows objects on a scene.
 * Authors: Wojciech Bruski, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
 **/

using System.Collections.Generic;
using UnityEngine;

public class HideShowUI : MonoBehaviour
{
    [SerializeField] private List<GameObject> objectsToHide = new List<GameObject>();

	/// <summary>
	/// Shows objects.
	/// </summary>
    public void ShowObjects()
    {
        foreach(GameObject obj in objectsToHide)
        {
            obj.SetActive(true);
        }
    }

	/// <summary>
	/// Hides objects.
	/// </summary>
    public void HideObjects()
    {
        foreach (GameObject obj in objectsToHide)
        {
            obj.SetActive(false);
        }
    }
}
