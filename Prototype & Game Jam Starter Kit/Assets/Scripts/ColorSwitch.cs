﻿/**
 * Description: Changes color of a sprite.
 * Authors: Wojciech Bruski, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
 **/

using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ColorSwitch : MonoBehaviour
{
    [SerializeField] private List<Color> colors = new List<Color>() { Color.red, Color.green, Color.blue };

    private int currentColor = 0;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.color = colors[currentColor];
    }

    public void ChangeColor()
    {
        if(++currentColor >= colors.Count)
        {
            currentColor = 0;
        }

        spriteRenderer.color = colors[currentColor];
    }
}
