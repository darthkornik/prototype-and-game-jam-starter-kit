﻿/**
 * Description: Script used to test (spawn) Floating Text.
 * Authors: Kornel
 * Copyright: © 2019 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using UnityEngine;
using UnityEngine.Assertions;

public class SpawnFloatingText : MonoBehaviour
{
	[SerializeField] private GameObject floatingText = null;

	void Start ()
	{
		Assert.IsNotNull( floatingText, $"Please assign <b>{nameof( floatingText )}</b> field on <b>{GetType( ).Name}</b> script on <b>{name}</b> object" );
	}

	public void Spawn( )
	{
		string textToShow = Random.Range( 1, 1000 ).ToString( );

		GameObject go = Instantiate( floatingText, transform.position, Quaternion.identity );
		FloatingText ft = go.GetComponent<FloatingText>( );
		ft.SetPrameters( textToShow );
	}

	public void SpawnRandom ()
	{
		string textToShow = Random.Range( 1, 1000 ).ToString( );
		float size = Random.Range( 1f, 2f );
		float speed = Random.Range( 1f, 2f );
		Color color = new Color( Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), Random.Range( 0f, 1f ) );

		GameObject go = Instantiate( floatingText, transform.position, Quaternion.identity );
		FloatingText ft = go.GetComponent<FloatingText>( );
		ft.SetPrameters( textToShow, size, speed, color);
	}
}
