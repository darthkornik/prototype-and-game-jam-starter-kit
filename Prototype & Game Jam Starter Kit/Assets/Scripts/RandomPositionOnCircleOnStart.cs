﻿/**
 * Description: Script used to test RandomPoint.OnCircle method (and it's overloads).
 * Authors: Paweł, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
 * */

using UnityEngine;

public class RandomPositionOnCircleOnStart : MonoBehaviour
{
    [SerializeField] private float radius = 1f;
    [SerializeField] private Vector3 center = Vector3.zero;

    void Start()
    {
        GetComponent<Rigidbody2D>().transform.position = RandomPoint.OnCircle(center, radius);
    }
}
