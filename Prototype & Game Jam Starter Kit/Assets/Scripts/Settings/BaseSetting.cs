using UnityEngine;

public abstract class BaseSetting<T> : MonoBehaviour, ISetting<T>
{
	public SettingsTab SettingsTab;
	protected ISettingApplicable<T> Applicable;
	[field: SerializeField] public SettingName Name { get; set; }
	[field: SerializeField] public virtual T CurrentValue { get; set; }
	[field: SerializeField] public T DefaultValue { get; set; }
	[field: SerializeField] public T SavedValue { get; set; }
	public virtual void OnValueChanged() => SettingsTab.SettingsWindow.OnValueChanged();
	public virtual void Apply() => SavedValue = CurrentValue;
	public virtual void SetCurrentValue(T value) => CurrentValue = value;
	public bool HasChanged() => (dynamic)SavedValue != CurrentValue;
	
	public void LoadData(T value)
	{
		SavedValue = value;
		CurrentValue = value;
	}
	public virtual void Initialize(SettingsTab tab) => SettingsTab = tab;
}
