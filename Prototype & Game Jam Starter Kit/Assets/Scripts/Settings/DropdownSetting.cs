using System;
using TMPro;
using UnityEngine;

public class DropdownSetting : BaseSetting<int>
{
	[field: SerializeField] public TMP_Dropdown SettingDropdown { get; private set; }

	private void Start()
	{
		SettingDropdown.onValueChanged.AddListener((x) => OnValueChanged());
	}

	public override int CurrentValue 
	{
		get
		{
			return SettingDropdown.value;
		}
		set
		{
			SettingDropdown.SetValueWithoutNotify(value);
			OnValueChanged();
		}
	}
}
