using UnityEngine;
using UnityEngine.UI;

public class SliderSetting : BaseSetting<float>
{
	[field: SerializeField] public Slider SettingSlider;

	private void Start()
	{
		SettingSlider.onValueChanged.AddListener((x) => OnValueChanged());
	}

	public override float CurrentValue
	{
		get
		{
			return SettingSlider.value;
		}
		set
		{
			SettingSlider.SetValueWithoutNotify(value);
			OnValueChanged();
		}
	}

	public void SetMaxAndMinValuesOfSlider(float maximumValue, float minimumValue)
	{
		SettingSlider.maxValue = maximumValue;
		SettingSlider.minValue = minimumValue;
	}

}
