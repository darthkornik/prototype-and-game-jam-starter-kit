using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Audio;
using UnityEngine.Rendering.Universal;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager Instance { get; private set; }
    public CameraControllerSettings CameraControllerSettings;
    public bool AllowCameraOnEdgeOverUIMovement = true;
    public SettingsInfo CurrentSettings { get; private set; } = new SettingsInfo();
    public SettingsInfo DefaultSettings => _defaultSettings;
    [field: SerializeField] public SettingsTab CurrentOpenedSettingsTab { get; private set; }
    [field: SerializeField] public List<SettingsTab> SettingsTabs { get; private set; }

    [field: SerializeField] public AudioMixer MasterMixer { get; private set; }
    [field: SerializeField] public UniversalRendererData Renderer { get; private set; }
    [field: SerializeField] public UniversalRenderPipelineAsset CurrentGraphicsSettings { get; private set; }
    
    [SerializeField] private ScriptableRendererFeature _occlusion;
    [SerializeField] private SettingsInfo _defaultSettings = new SettingsInfo();
    [SerializeField] private AudioSettings _audioSettings;
    [SerializeField] private List<SettingsWindow> _settingsWindows = new List<SettingsWindow>();

    public ScriptableRendererFeature Occlusion => _occlusion;

    private void Awake()
    {
        Assert.IsNotNull(_audioSettings);

        if ( Instance != null && Instance != this )
        {
            //Destrying this duplicated singleton, but that is good, becouse once initialized singletone is "DontDestroyOnLoad"
            //Debug.LogError( $"Duplicate singelton {nameof( SettingsManager )} on {gameObject.name}" );
            Destroy( gameObject );
        }
        else
            Instance = this;

        DontDestroyOnLoad(gameObject);
        ApplySettings(LoadSettings());
        InitializeSettingsTabs();
        foreach (SettingsTab tab in SettingsTabs) tab.OnSettingsTabOpen += GetCurrentSettingsTab;
    }
    
    void OnDestroy()
    {
        if ( this == Instance ) Instance = null;
        foreach (SettingsTab tab in SettingsTabs) tab.OnSettingsTabOpen -= GetCurrentSettingsTab;
    }

    public void ApplySettings(SettingsInfo settings)
    {
        CurrentSettings = settings;
        SaveSettings(CurrentSettings);

        Resolution resolution = Resolutions()[settings.Resolution];
        Screen.SetResolution(resolution.width, resolution.height, (FullScreenMode)settings.ScreenMode);
        _audioSettings.LateApplyParameters(settings);
        
    }

    private void InitializeSettingsTabs()
    {
        foreach (SettingsTab tab in SettingsTabs)
        {
            if (tab.SettingsWindowName == "Graphics Tab")
            {
                tab.Initialize(_settingsWindows[0]);
                continue;
            }

            if (tab.SettingsWindowName == "Sound Tab")
            {
                tab.Initialize(_settingsWindows[1]);
            }
        }
    }

    public void SaveSettings(SettingsInfo settings)
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsInfo));
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, settings);
                PlayerPrefs.SetString("Settings", writer.ToString());
            }
        }
        catch
        {
            Debug.LogError("Could not save settings");
        }

        foreach (var tab in SettingsTabs) tab.SaveTabSettings(tab.SettingsTabName + "/");
    }

    public SettingsInfo LoadSettings()
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsInfo));
            string txt = PlayerPrefs.GetString("Settings");
            if (string.IsNullOrEmpty(txt))
            {
                return _defaultSettings;
            }
            else
            {
                using (StringReader reader = new StringReader(txt))
                {                
                    return serializer.Deserialize(reader) as SettingsInfo;
                }
            }
        }
        catch
        {
            Debug.LogError("Could not load settings");
        }

        foreach (var tab in SettingsTabs) tab.LoadTabSettings(tab.SettingsTabName + "/");
        return _defaultSettings;
    }

    public List<Resolution> Resolutions()
    {
        List<Resolution> resolutions = Screen.resolutions.ToList();
        resolutions.Reverse();

        return resolutions;
    }

    public void OnOptionsWindowDisabled() => CurrentOpenedSettingsTab = null;

    private void GetCurrentSettingsTab(SettingsTab tab) => CurrentOpenedSettingsTab = tab;
    
}
