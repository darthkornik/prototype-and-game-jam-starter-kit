using UnityEngine;
using UnityEngine.UI;

public class ToggleSetting : BaseSetting<bool>
{
	[field: SerializeField] public Toggle SettingToggle { get; private set; }

	private void Start()
	{
		SettingToggle.onValueChanged.AddListener((x) => OnValueChanged());
	}

	public override bool CurrentValue
	{
		get
		{
			return SettingToggle.isOn;
		}
		set
		{
			SettingToggle.SetIsOnWithoutNotify(value);
			OnValueChanged();
		}
	}
}
