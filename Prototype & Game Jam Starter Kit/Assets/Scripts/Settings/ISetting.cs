public interface ISetting<T>
{
	public SettingName Name { get; set; }
	public T CurrentValue { get; set; }
	public T DefaultValue { get; set; }
	public T SavedValue { get; set; }
	public void OnValueChanged();
	public void SetCurrentValue(T value);
	public void Apply();
	public void LoadData(T value);
	public void Initialize(SettingsTab tab);
	public bool HasChanged();
	
}
