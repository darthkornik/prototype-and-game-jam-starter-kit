using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
 
public class SettingsTab : MonoBehaviour
{
	[SerializeField] private Transform _parentLayout;
	[field: SerializeField] public List<ISetting<float>> FloatValues { get; private set; } = new List<ISetting<float>>();
	[field: SerializeField] public List<ISetting<int>> IntValues { get; private set; } = new List<ISetting<int>>();
	[field: SerializeField] public List<ISetting<bool>> BoolValues { get; private set; } = new List<ISetting<bool>>();

	public event Action<SettingsTab> OnSettingsTabOpen;


	public SettingsWindow SettingsWindow { get; private set; }
	public string SettingsWindowName;
	public string SettingsTabName;

	private void OnEnable() => OnSettingsTabOpen?.Invoke(this);
	public void Initialize(SettingsWindow settingsWindow)
	{
		SettingsWindow = settingsWindow;
		
		FloatValues = _parentLayout.GetComponentsInChildren<ISetting<float>>().ToList();
		IntValues = _parentLayout.GetComponentsInChildren<ISetting<int>>().ToList();
		BoolValues = _parentLayout.GetComponentsInChildren<ISetting<bool>>().ToList();
		
		foreach (var setting in FloatValues) setting.Initialize(this);
		foreach (var setting in IntValues) setting.Initialize(this);
		foreach (var setting in BoolValues) setting.Initialize(this);
	}

	public void SaveTabSettings(string prefix)
	{
		foreach (var setting in FloatValues) SaveValue(prefix, setting);
		foreach (var setting in IntValues) SaveValue(prefix, setting);
		foreach (var setting in BoolValues) SaveValue(prefix, setting);
	}

	public void LoadTabSettings(string prefix)
	{
		foreach (var setting in FloatValues) LoadValue(prefix, setting);
		foreach (var setting in IntValues) LoadValue(prefix, setting);
		foreach (var setting in BoolValues) LoadValue(prefix, setting);
	}
	
	public void ApplyTabSettings()
	{
		foreach (var setting in FloatValues) setting.Apply();
		foreach (var setting in IntValues) setting.Apply();
		foreach (var setting in BoolValues) setting.Apply();		
	}

	public void SetAllTabSettingsValuesToDefault()
	{
		foreach (var setting in FloatValues) setting.SetCurrentValue(setting.DefaultValue);	
		foreach (var setting in IntValues) setting.SetCurrentValue(setting.DefaultValue);	
		foreach (var setting in BoolValues) setting.SetCurrentValue(setting.DefaultValue);
	}

	public void RevertTabSettingsValues()
	{
		foreach (var setting in FloatValues) setting.SetCurrentValue(setting.SavedValue);	
		foreach (var setting in IntValues) setting.SetCurrentValue(setting.SavedValue);	
		foreach (var setting in BoolValues) setting.SetCurrentValue(setting.SavedValue);
	}

	public bool HasChanged()
	{
		foreach (var setting in FloatValues) if (setting.HasChanged()) return true;
		foreach (var setting in IntValues) if(setting.HasChanged()) return true;	
		foreach (var setting in BoolValues) if(setting.HasChanged()) return true;
		return false;
	}

	public BaseSetting<T> GetBaseSetting<T>(SettingName settingName)
	{
		if (typeof(T) == typeof(float))
		{
			BaseSetting<T> setting = (dynamic)FloatValues.Find(setting => setting.Name == settingName);
			if (setting == null)
			{
				throw new ArgumentException($"Cannot find {settingName} in list Float Values, it must be on one of th sliders");
			}
			return setting;
		}
		
		if (typeof(T) == typeof(int))
		{
			BaseSetting<T> setting = (dynamic)IntValues.Find(setting => setting.Name == settingName);
			if (setting == null)
			{
				throw new ArgumentException($"Cannot find {settingName} in list Int Values, it must be on one of the dropdowns");
			}
			return setting;
			
		}
		
		if (typeof(T) == typeof(bool))
		{
			BaseSetting<T> setting = (dynamic)BoolValues.Find(setting => setting.Name == settingName);
			if (setting == null)
			{
				throw new ArgumentException($"Cannot find {settingName} in list Bool Values, it must be on one of the toggles");
			}
			return setting;
		}

		return default;
	}

	public BaseSetting<T> GetSetting<T>(BaseSetting<T> setting)
	{
		if (setting == null)
		{
			throw new Exception($"Setting {setting} (argument of method) is null" );
		}
		
		if (typeof(T) == typeof(float))
		{
			return (dynamic)GetSliderSetting(setting);
		}
		
		if (typeof(T) == typeof(int))
		{
			return (dynamic)GetDropdownSetting(setting);
		}
		
		if (typeof(T) == typeof(bool))
		{
			return (dynamic)GetToggleSetting(setting);
		}

		return default;
	}

	private SliderSetting GetSliderSetting<T>(BaseSetting<T> setting)
	{
		SliderSetting sliderSetting = (dynamic)setting;
		return sliderSetting;
	}

	private ToggleSetting GetToggleSetting<T>(BaseSetting<T> setting)
	{
		ToggleSetting toggleSetting = (dynamic)setting;
		return toggleSetting;
	}

	private DropdownSetting GetDropdownSetting<T>(BaseSetting<T> setting)
	{
		DropdownSetting dropdownSetting = (dynamic)setting;
		return dropdownSetting;
	}

	private void SaveValue<T>(string prefix,ISetting<T> setting)
	{
		if (typeof(T) == typeof(float))
		{
			PlayerPrefs.SetFloat(prefix + setting.Name, (dynamic)setting.CurrentValue);
			return;
		}
		
		if (typeof(T) == typeof(int))
		{
			PlayerPrefs.SetInt(prefix + setting.Name, (dynamic)setting.CurrentValue);
			return;
		}
		
		if (typeof(T) == typeof(bool))
		{
			PlayerPrefs.SetInt(prefix + setting.Name, (dynamic)setting ? 1 : 0);
		}
	}

	private void LoadValue<T>(string prefix,ISetting<T> setting)
	{
		if (typeof(T) == typeof(float))
		{
			setting.CurrentValue = PlayerPrefs.GetFloat(prefix + setting.Name, (dynamic)setting.DefaultValue);
			return;
		}
		
		if (typeof(T) == typeof(int))
		{
			setting.CurrentValue = PlayerPrefs.GetInt(prefix + setting.Name, (dynamic)setting.DefaultValue);
			return;
		}
		
		if (typeof(T) == typeof(bool))
		{
			int value = (PlayerPrefs.GetInt(prefix + setting.Name, (dynamic)setting.DefaultValue));
			setting.CurrentValue = (dynamic)(value == 1);
		}
	}
}
