using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsWindow : MonoBehaviour
{
	[SerializeField] private Button _apply;
	[SerializeField] private GameObject _settingsAppliedAnimation = null;
	[SerializeField] private Button _graphicsSettingsOpenButton;
	[SerializeField] private Button _generalSettingsOpenButton;
	[SerializeField] private SettingsTab _settingsTab;
	private SettingsManager _settingsManager = null;

	void Start()
	{
		_settingsManager = SettingsManager.Instance;
		RevertValues();
	}

    public void RevertValues() => RevertValues(_settingsManager.CurrentSettings);
	private void RevertValues(SettingsInfo settings)
    {
	    OnValueChanged();
    }

	//Used by button
	public void ApplyButton()
    {
		_settingsAppliedAnimation.SetActive(false);
		_settingsAppliedAnimation.SetActive(true);
		_settingsTab.ApplyTabSettings();
		OnValueChanged();
		if (ShipLoader.Instance != null) ShipLoader.Instance.EnableCorrectWaterWake();
    }

	//Used by button
	public void CancelButton()
	{
		if (!SettingsChanged())
		{
			CancelSettings();
			return;
		}

		ModalWindow areYouSure = new ModalWindow()
		{
			Title = "Are you sure to cancel settings",
			Description = "",
			PauseGame = true,
			Callback = (button) => CancelSettings(button),
			WindowType = ModalWindowType.YesNo,
		};

		ModalWindowsManager.Instance.ShowModalWindow(areYouSure);
	}

	//Used by button
	public void SetDefaultButton()
	{
		ModalWindow areYouSure = new ModalWindow()
		{
			Title = "Are you sure to reset settings to default",
			Description = "",
			PauseGame = true,
			Callback = (button) => SetDefaultSettings(button),
			WindowType = ModalWindowType.YesNo,
		};
		ModalWindowsManager.Instance.ShowModalWindow(areYouSure);
	}
	
	private void SetDefaultSettings(ButtonType type)
    {
		if (type != ButtonType.Yes) return;
		_settingsManager.CurrentOpenedSettingsTab.SetAllTabSettingsValuesToDefault();
    }

	private void CancelSettings(ButtonType type = ButtonType.Yes)
    {
		if (type != ButtonType.Yes) return;

		_settingsAppliedAnimation.SetActive(false);
		gameObject.SetActive(false);
		_generalSettingsOpenButton.gameObject.SetActive(false);
		_graphicsSettingsOpenButton.gameObject.SetActive(false);
		_settingsTab.RevertTabSettingsValues();
		_settingsManager.OnOptionsWindowDisabled();
    }

	public void OnValueChanged()
    {
		_apply.interactable = SettingsChanged();
    }

    private bool SettingsChanged()
    {
	    if (_settingsTab.HasChanged())
		    return true;
	    return false;
    }
}
