/**
 * Description: Tool for checking propriety of fields
 * Authors: Micha�, Rebel Game Studio
 * Copyright: � 2022 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
 **/

using System.Collections;
using System.Diagnostics;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;

public static class FieldChecker
{
    private const BindingFlags bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;

    [Conditional("UNITY_EDITOR")]
    public static void IsNotNull(object caller, params string[] fieldName)
    {
        foreach (var field in fieldName)
        {
            IsNotNull(caller, field);
        }
    }

    [Conditional("UNITY_EDITOR")]
    public static void IsNotNull(object caller, string fieldName)
    {
        CheckIsNotNull(caller, fieldName);
    }

    private static bool CheckIsNotNull(object caller, string fieldName)
    {
        var field = GetField(caller, fieldName);
        if (field != null && field.GetValue(caller) != null && !field.GetValue(caller).Equals(null)) return true;

        var property = GetPropertyInfo(caller, fieldName);
        if (property != null && property.GetValue(caller)!= null && !property.GetValue(caller).Equals(null)) return true;

        PrintError($"Field <b>{fieldName}</b> is null on script <b>{caller.GetType()}</b>", caller as Object);
        return false;
    }


    [Conditional("UNITY_EDITOR")]
    public static void ArrayIsNotEmpty(object caller, params string[] arrays)
    {
        foreach (var field in arrays)
        {
            ArrayIsNotEmpty(caller, field);
        }
    }

    [Conditional("UNITY_EDITOR")]
    public static void ArrayIsNotEmpty(object caller, string fieldName)
    {
        if (!CheckIsNotNull(caller, fieldName)) return;

        IEnumerator array = GetMemberEnumerator(fieldName, caller);
        if (array == null)
        {
            PrintError($"Field <b>{fieldName}</b> in <b>{caller.GetType()}</b> is not an array", caller as Object);
            return;
        }

        if (array.MoveNext()) return;
        PrintError($"Length of array <b>{fieldName}</b> is <b>0</b> on script <b>{caller.GetType()}</b>", caller as Object);
    }

    private static void PrintError(string message, Object caller = null)
    {
        if (caller is not null)
        {
            message += $" on <b>{caller.name}</b> object\n";
            UnityEngine.Debug.LogError(message, caller);
            return;
        }

        UnityEngine.Debug.LogError(message);
    }

    private static IEnumerator GetMemberEnumerator(string name, object caller)
    {
        FieldInfo field = GetField(caller,name);
        if (field != null) return ((IEnumerable)field.GetValue(caller)).GetEnumerator();

        PropertyInfo property = GetPropertyInfo(caller,name);
        if (property != null) return ((IEnumerable)property.GetValue(caller)).GetEnumerator();

        UnityEngine.Debug.LogError("Getting IEnumerator from member unsuccesful");
        return null;
    }


    private static PropertyInfo GetPropertyInfo(object parentObject, string variableName)
    {
        PropertyInfo property = parentObject.GetType().GetProperty(variableName, bindingFlags);

        if (property != null) return property;
        return null;
    }

    private static FieldInfo GetField(object parentObject, string variableName)
    {
        FieldInfo field = parentObject.GetType().GetField(variableName, bindingFlags);

        if (field != null) return field;
        return null;
    }
}
