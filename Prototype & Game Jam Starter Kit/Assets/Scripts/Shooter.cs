﻿/**
 * Description: Shoots objects using 2D physics.
 * Authors: Kornel, Rebel Game Studio
 * Copyright: © 2020 Rebel Game Studio.All rights reserved.For license see: 'LICENSE' file.
 **/

using UnityEngine;
using UnityEngine.Assertions;

public class Shooter : MonoBehaviour
{
	[SerializeField] private GameObject objectToShoot = null;
	[SerializeField] private Transform spawnPoint = null;
	[SerializeField] private float shootingCooldown = 2f;
	[SerializeField] private float shootingStrength = 2f;

	void Start( )
	{
		Assert.IsNotNull( objectToShoot, $"Please assign <b>{nameof( objectToShoot )}</b> field on <b>{GetType( ).Name}</b> script on <b>{name}</b> object" );
		Assert.IsNotNull( spawnPoint, $"Please assign <b>{nameof( spawnPoint )}</b> field on <b>{GetType( ).Name}</b> script on <b>{name}</b> object" );

		Shoot( );
	}

	private void Shoot( )
	{
		GameObject go = Instantiate(objectToShoot, spawnPoint.position, Quaternion.identity);
		go.GetComponent<Rigidbody2D>( ).AddForce( transform.up * shootingStrength );

		Invoke( nameof( Shoot ), shootingCooldown );
	}
}
