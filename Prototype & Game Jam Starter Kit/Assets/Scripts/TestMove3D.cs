﻿/**
 * Description: Simple physics movement script - add to an object to make it move like a spaceship.
 * Authors: Michał, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
**/

using UnityEngine;
using UnityEngine.Assertions;

public class TestMove3D : MonoBehaviour
{
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Assert.IsNotNull(rb);
    }

	void FixedUpdate ()
	{
		RotateAndAddForce( );
	}

	private void RotateAndAddForce( )
	{
		transform.eulerAngles = new Vector3( transform.eulerAngles.x + Time.deltaTime * -Input.GetAxis( "Vertical" ) * 100,
					transform.eulerAngles.y + Time.deltaTime * Input.GetAxis( "Horizontal" ) * 100, transform.eulerAngles.z );

		if ( Input.GetKey( KeyCode.Space ) )
		{
			rb.AddForce( transform.forward * 5 );
		}
	}
}
