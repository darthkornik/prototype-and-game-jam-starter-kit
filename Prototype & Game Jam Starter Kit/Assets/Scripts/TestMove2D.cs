﻿/**
 * Description: Simple script for movement, for testing..
 * Authors: Michał Wildanger, Rebel Game Studio
 * Copyright: © 2018-2019 Rebel Game Studio. All rights reserved. For license see: 'LICENSE' file.
**/

using UnityEngine;

public class TestMove2D : MonoBehaviour
{
	void FixedUpdate ()
	{
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += new Vector3(-3,0,0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(3, 0, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(0, 3, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position += new Vector3(0, -3, 0) * Time.deltaTime;
        }
    }
}
