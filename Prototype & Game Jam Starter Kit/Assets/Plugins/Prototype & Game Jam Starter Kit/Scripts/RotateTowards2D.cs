﻿/**
 * Description: Rotates towards target or mouse position.
 * Authors: Kornel
 * Copyright: © 2019 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using UnityEngine;

public class RotateTowards2D : MonoBehaviour
{
	[SerializeField] private Transform target = null;
	[SerializeField] private float speed = 100f;

	void Update( )
	{
		RotateTowardsPlayer( );
	}

	private void RotateTowardsPlayer( )
	{
		Vector2 targetPosition;

		if ( target )
			targetPosition = target.position;
		else
			targetPosition = Camera.main.ScreenToWorldPoint( Input.mousePosition );

		float angleRad = Mathf.Atan2( targetPosition.y - transform.position.y, targetPosition.x - transform.position.x );
		float angleDeg = angleRad * Mathf.Rad2Deg;
		float endAngle = Quaternion.Euler( 0, 0, angleDeg ).eulerAngles.z;
		float currentAngle = transform.rotation.eulerAngles.z;
		float angleDifference = Mathf.DeltaAngle( currentAngle, endAngle );

		if (angleDifference > 0 )
		{
			float newRotationAngle = currentAngle + speed * Time.deltaTime;
			newRotationAngle = newRotationAngle < currentAngle + angleDifference ? newRotationAngle : currentAngle + angleDifference;
			transform.rotation = Quaternion.Euler( 0, 0, newRotationAngle );
		}
		else if ( angleDifference < 0 )
		{
			float newRotationAngle = currentAngle - speed * Time.deltaTime;
			newRotationAngle = newRotationAngle > currentAngle + angleDifference ? newRotationAngle : currentAngle + angleDifference;
			transform.rotation = Quaternion.Euler( 0, 0, newRotationAngle );
		}
	}
}
