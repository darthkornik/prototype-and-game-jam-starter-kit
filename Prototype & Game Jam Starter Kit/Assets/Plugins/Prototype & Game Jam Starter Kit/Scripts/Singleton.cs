﻿/**
 * Description: Universal singelton base class.
 * Authors: Kornel
 * Copyright: © 2020-2022 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	public static T Instance { get; private set; }

	public virtual void Awake()
	{
		if(Instance != null && Instance != this)
		{
			Debug.LogError($"Duplicate singleton {nameof(T)} on {gameObject.name}");
			Destroy(this);

			return;
		}

		Instance = GetComponent<T>();
	}

	public virtual void OnDestroy()
	{
		if(this == Instance)
			Instance = null;
	}
}
