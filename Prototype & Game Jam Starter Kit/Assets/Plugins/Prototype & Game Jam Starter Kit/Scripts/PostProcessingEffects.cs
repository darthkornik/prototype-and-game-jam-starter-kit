﻿/**
 * Description: Quick post-processing effects using code.
 * Authors: Kornel
 * Copyright: © 2019 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingEffects : MonoBehaviour
{
	public static PostProcessingEffects Instance { get; private set; }

	[SerializeField] private PostProcessVolume volume = null;
	[SerializeField] private float explosionBloom = 4f;
	[SerializeField] private float explosionChromatic = 1f;
	[SerializeField] private float returnTime = 1f;

	private Bloom bloomLayer = null;
	private ChromaticAberration chromaticLayer = null;

	private float normalBloom = 1f;
	private float normalChromatic = 0f;

	private void Awake( )
	{
		if ( Instance != null && Instance != this )
			Destroy( this );
		else
			Instance = this;
	}

	private void OnDestroy( ) { if ( this == Instance ) { Instance = null; } }

	void Start( )
	{
		if ( !volume )
			volume = FindObjectOfType<PostProcessVolume>( );

		Assert.IsNotNull( volume, $"Please assign <b>{nameof( volume )}</b> field on <b>{GetType( ).Name}</b> script on <b>{name}</b> object" );

		volume.profile.TryGetSettings( out bloomLayer );
		volume.profile.TryGetSettings( out chromaticLayer );

		normalBloom = bloomLayer.intensity.value;
		normalChromatic = chromaticLayer.intensity.value;
	}

	public void Explosion( float strenght )
	{
		float newExpBloom = explosionBloom * strenght;
		float newExpChromatic = explosionChromatic * strenght;

		// Skip if new effect is weaker then the current values of the old one
		if ( bloomLayer.intensity.value >= newExpBloom )
			return;

		bloomLayer.intensity.value = newExpBloom;
		chromaticLayer.intensity.value = newExpChromatic;

		//LeanTween.cancel( gameObject );
		//LeanTween.value( gameObject, 0f, 1f, returnTime ).setOnUpdate( v => ReturnValuesToNormal( v ) );
		StopAllCoroutines( );
		StartCoroutine( ChangeOverTime( returnTime, ReturnValuesToNormal ) );
	}

	private void ReturnValuesToNormal( float percent )
	{
		bloomLayer.intensity.value = Mathf.Lerp( explosionBloom, normalBloom, percent );
		chromaticLayer.intensity.value = Mathf.Lerp( explosionChromatic, normalChromatic, percent );
	}

	private IEnumerator ChangeOverTime( float timeLength, System.Action<float> methodWithParameter )
	{
		float currentTime = 0;
		float durationTime = timeLength;

		float progress;
		do
		{
			currentTime += Time.deltaTime;
			currentTime = currentTime < durationTime ? currentTime : durationTime;

			progress = currentTime / durationTime;
			progress = progress < 1.0f ? progress : 1.0f;

			methodWithParameter( progress );

			yield return null;
		}
		while ( progress < 1 );
		// And we are done
	}
}
