﻿/**
 * Description: A collection of useful extension methods.
 * Authors: Kornel
 * Inspired by: https://www.reddit.com/r/Unity3D/comments/d39s7k/did_you_know_that_theres_an_easy_way_to_improve/
 * Copyright: © 2019 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using UnityEngine;

public static class LogEvents
{
	/// <summary>
	/// Logs an event to the console.
	/// </summary>
	/// <param name="caller">Script that is calling</param>
	/// <param name="subject">Who is calling (for example name of the GameObject or game entity).</param>
	/// <param name="whatHappend">What event occurred ("got damaged", "killed", etc.)</param>
	/// <param name="amount">Optional amount of change (for example how much damage something took).</param>
	/// <param name="positive">Is this a positive occurrence (healing) or negative (damage).</param>
	public static void LogEvent( this MonoBehaviour caller, string subject, string whatHappend, float amount = float.NegativeInfinity, bool positive = true )
	{
		if ( amount == float.NegativeInfinity )
			Debug.Log( $"<size=22><color=#2864CB>{subject}</color> <color=#272727><b>{whatHappend}</b></color>.</size>", caller );

		if ( amount != float.NegativeInfinity && !positive )
			Debug.Log( $"<size=22><color=#2864CB>{subject}</color> <color=#272727><b>{whatHappend}</b> for </color><b><color=#F10E00>{amount}</color></b>.</size>", caller );

		if ( amount != float.NegativeInfinity && positive )
			Debug.Log( $"<size=22><color=#2864CB>{subject}</color> <color=#272727><b>{whatHappend}</b> for </color><b><color=#198A02>{amount}</color></b>.</size>", caller );
	}

	/// <summary>
	/// Logs a simple event to the console.
	/// </summary>
	/// <param name="caller"></param>
	/// <param name="whatHappend">What event occurred ("I got damaged", "Job completed", etc.)</param>
	/// <param name="bold">Should the text be bold?</param>
	/// <param name="italic">Should the text be italic?</param>
	public static void LogEvent( this MonoBehaviour caller, string whatHappend, bool bold = false, bool italic = false )
	{
		if ( bold && !italic )
			Debug.Log( $"<size=22><color=#272727><b>{whatHappend}.</b></color></size>", caller );
		else if ( !bold && italic )
			Debug.Log( $"<size=22><color=#272727><i>{whatHappend}.</i></color></size>", caller );
		else if( bold && italic )
			Debug.Log( $"<size=22><color=#272727><i><b>{whatHappend}.</b></i></color></size>", caller );
		else
			Debug.Log( $"<size=22><color=#272727>{whatHappend}.</color></size>", caller );
	}
}
