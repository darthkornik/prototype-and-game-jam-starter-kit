﻿/**
 * Description: Shows and/or hides GameObjects. Does it on OnEnable and/or OnDisable.
 * Authors: Kornel
 * Copyright: © 2019 Kornel. All rights reserved. For license see: 'LICENSE.txt'
 **/

using UnityEngine;

public class ShowHide : MonoBehaviour
{
	[Header("To Show")]
	[SerializeField] private GameObject[] ToShowOnEnabled = null;
	[SerializeField] private GameObject[] ToShowOnDisabled = null;

	[Header("To Hide")]
	[SerializeField] private GameObject[] ToHideOnEnabled = null;
	[SerializeField] private GameObject[] ToHideOnDisabled = null;

	private void OnEnable( )
	{
		foreach ( var item in ToShowOnEnabled )
			item.SetActive( true );

		foreach ( var item in ToHideOnEnabled )
			item.SetActive( false );
	}

	private void OnDisable( )
	{
		foreach ( var item in ToShowOnDisabled )
			item.SetActive( true );

		foreach ( var item in ToHideOnDisabled )
			item.SetActive( false );
	}
}
