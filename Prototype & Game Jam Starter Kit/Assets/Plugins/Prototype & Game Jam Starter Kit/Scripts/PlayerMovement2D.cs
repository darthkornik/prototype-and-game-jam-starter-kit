﻿/**
 * Description: Basic character/player movement for 2D top-down or similar games.
 * Authors: Kornel, Rebel Game Studio
 * Copyright: © 2020 Rebel Game Studio.All rights reserved.For license see: 'LICENSE' file.
 **/

/**
 * For best movement experience please set Axes parameters (in Input Manager):
 * - Gravity = 3000
 * - Sensitivity = 300
 **/

using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement2D : MonoBehaviour
{
	public bool CanMove { get; set; } = true;

	[Header("Speed")]
	[SerializeField, Tooltip("Energy applied. Think acceleration.")] private float moveForce = 100f;
	[SerializeField, Tooltip("Max speed of movement. Think speed limit.")] private float maxVelocity = 10f;
	[SerializeField, Range(0f, 50f), Tooltip("How much velocity is retained while not moving. 50 = 100%, 40 = 80%. The smaller number the faster it will halt to a stop. Think deceleration.")] private float velocityRetained = 40f;

	[Header("Physics Parameters (overrides Rigidbody settings!)")]
	[SerializeField] private bool enableOverride = true;
	[SerializeField] private float drag = 7f;
	[SerializeField] private float gravityScale = 0f;
	[SerializeField] private bool freezeRotation = true;

	private Rigidbody2D rb2D = null;
	private Vector2 input = Vector2.zero;
	private Vector2 inputRaw = Vector2.zero;

	void Start( )
	{
		rb2D = GetComponent<Rigidbody2D>( );
		Assert.IsNotNull( rb2D, $"Please assign <b>{nameof( rb2D )}</b> field on <b>{GetType( ).Name}</b> script on <b>{name}</b> object" );

		SetUp( );
	}

	void Update( )
	{
		GetInput( );
	}

	void FixedUpdate( )
	{
		Move( );
	}

	private void SetUp( )
	{
		if ( !enableOverride )
			return;

		rb2D.gravityScale = gravityScale;
		rb2D.drag = drag;
		rb2D.freezeRotation = freezeRotation;
	}

	private void GetInput( )
	{
		input.x = Input.GetAxis( "Horizontal" );
		input.y = Input.GetAxis( "Vertical" );
		inputRaw.x = Input.GetAxisRaw( "Horizontal" );
		inputRaw.y = Input.GetAxisRaw( "Vertical" );

		if ( input.magnitude > 1.0f )
			input.Normalize( );
	}

	private void Move( )
	{
		if ( !CanMove )
			return;

		if ( inputRaw.sqrMagnitude <= 0 )
			rb2D.velocity *= velocityRetained * Time.fixedDeltaTime;

		Vector2 newForce = input * moveForce * Time.fixedDeltaTime;
		rb2D.AddForce( newForce, ForceMode2D.Impulse );

		if ( rb2D.velocity.magnitude > maxVelocity )
			rb2D.velocity = rb2D.velocity.normalized * maxVelocity;
	}
}
